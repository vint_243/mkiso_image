#!/usr/bin/env bash

repo="/mnt/data/Server/Devel/Vintoo/src/fs"
repo_i="mnt/data/Server/Devel/Vintoo/src/fs"
image_fs="$repo/image.fs"
iso_fs="$repo_i/iso"
iso_done="$repo/iso"

name_project="vintoo"

echo
echo "This script from build iso image"
echo

echo
echo "Current project name: $name_project"
echo

echo 
echo "Remove old squashfs image_fs"
echo

sudo rm -v $image_fs/build/image.squashfs*

echo
echo "Build squashfs image_fs "
echo

sudo mksquashfs $image_fs/unpack/ $image_fs/build/image.squashfs  -progress -comp xz

cd $image_fs/build/ && md5sum image.squashfs > ./image.squashfs.md5

echo
echo "Copy new squashfs image_fs from src iso path"
echo

rm -v $iso_done/src/image.squashfs{,.md5}

echo

cp -v $image_fs/build/image.squashfs{,.md5} $iso_done/src/

reliase_path="$iso_done/compile"

echo
echo "Current reliase "
echo

cd $reliase_path 
current_relise=`ls *.iso ` 

echo $current_relise

echo

echo
echo "Enter new reliase number from this build" ; read reliase_num
echo

echo "Current reliase: $reliase_num"

echo
echo "Build iso image "
echo

cd $iso_done/src/ && sudo xorriso -as mkisofs -r -J -joliet-long -l -cache-inodes -isohybrid-mbr /usr/share/syslinux/isohdpfx.bin -partition_offset 16 -A 'Vintoo Live DVD'  -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -o $reliase_path/$name_project-$reliase_num.iso ./ 

cd $reliase_path/ && md5sum $name_project-$reliase_num.iso > ./$name_project-$reliase_num.iso.md5

echo
echo "Build image done"
echo

image_fs_size=`du -sh $iso_done/src/image.squashfs`
image_fs_sum=`cat $iso_done/src/image.squashfs.md5 | awk ' {print $1} '`

image_iso_size=`du -sh $reliase_path/$name_project-$reliase_num.iso`
image_iso_sum=`cat $reliase_path/$name_project-$reliase_num.iso.md5 | awk ' {print $1} '`

echo
echo "Current size image_fs: $image_fs_size"
echo

echo 
echo "Current md5sum image_fs: $image_fs_sum"
echo

echo
echo "Current size iso image: $image_iso_size"
echo

echo
echo "Current md5sum iso image: $image_iso_sum"
echo


